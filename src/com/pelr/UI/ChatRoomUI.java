package com.pelr.UI;

import com.pelr.domain.Message;
import com.pelr.service.ChatRoom;
import com.pelr.service.MessageNotFound;
import com.pelr.utils.Constants;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class ChatRoomUI {
    ChatRoom chatRoom;
    private boolean isOpen;

    public ChatRoomUI(ChatRoom chatRoom){
        this.chatRoom = chatRoom;
    }

    public void open(){
        isOpen = true;
        printRoomInformation();
        printConversation();
        printHelpInformation();
        while(isOpen){
            String message = promptSender();

            if(message.equals(Constants.CHATROOM_EXIT_COMMAND)){
                close();
                break;
            } else if(message.equals(Constants.CHATROOM_REPLY_COMMAND)){
                replyToMessage();
            }else {
                chatRoom.send(message);
            }
        }
    }

    private void printHelpInformation(){
        System.out.println("Type \"/exit\" to exit conversation.");
        System.out.println("Type \"/reply\" to reply to a message.");
        System.out.println();
    }

    private void replyToMessage(){
        System.out.println("Type the message number that you want to reply to: ");
        Scanner scanner = new Scanner(System.in);
        int messageIndex;

        try {
            messageIndex = Integer.parseInt(scanner.nextLine());
            System.out.println("Write reply message: ");
            String message = scanner.nextLine();
            chatRoom.reply(message, messageIndex);
        } catch (NumberFormatException e){
            System.out.println("Invalid index!");
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void printRoomInformation(){
        System.out.println();
        System.out.println("----------------------------");
        System.out.println("Chat with " + chatRoom.getReceiver().getFirstName() + " " + chatRoom.getReceiver().getLastName());
        System.out.println("----------------------------");

    }

    private String getMessageString(Message message){
        String messageString;

        if (message.getRepliedTo() == null) {
            messageString = message.getFrom().getFirstName() + " " + message.getFrom().getLastName() + ": " + message.getMessage() + "               " +
                    message.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        } else {
            messageString = message.getFrom().getFirstName() + " " + message.getFrom().getLastName() + ": " + message.getMessage() + "               " +
                    "(replied to " + chatRoom.getIndexOf(message.getRepliedTo()) + ") "
                    + message.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }

        return messageString;
    }

    private void printConversation(){
        List<Message> conversation = chatRoom.getMessages();

        for (int i = 0; i < conversation.size(); i++) {
            Message message = conversation.get(i);
            System.out.println(i + ": " + getMessageString(message) + "\n");
        }
    }

    private String promptSender(){
        System.out.println("Write a message: ");
        Scanner scanner = new Scanner(System.in);

        return scanner.nextLine();
    }

    public void close(){
        isOpen = false;
    }
}
