package com.pelr.UI;


import com.pelr.domain.DTOs.FriendRequestDTO;
import com.pelr.domain.DTOs.FriendshipDTO;

import com.pelr.domain.DTOs.ConversationHeaderDTO;

import com.pelr.domain.User;
import com.pelr.service.Controller;
import com.pelr.utils.Constants;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Console class that manages the UI.
 */
public class Console {
    private Controller controller;

    /**
     * Creates a new console bound to a controller.
     *
     * @param controller - Controller object that executes commands.
     */
    public Console(Controller controller) {
        this.controller = controller;
    }

    /**
     * Returns the application menu.
     *
     * @return menu - Iterable of strings representing the menu
     */
    private Iterable<String> generateMenu() {
        List<String> menu = new ArrayList<>();
        menu.add("1. Add user");
        menu.add("2. Remove user");
        menu.add("3. Display users");
        menu.add("4. Log in");
        menu.add("5. Log out");
        menu.add("6. Add friends");
        menu.add("7. Remove friends");
        menu.add("8. Display friends");
        menu.add("9. Number of communities");
        menu.add("10. Most sociable community");
        menu.add("11. Conversations");
        menu.add("12. Send a message to multiple users");
        menu.add("13. Show friend requests");
        menu.add("14. Show friendship from specific year and month");
        menu.add("0. Exit");

        return menu;
    }

    /**
     * Prints the options in console.
     *
     * @param options - the options to be printed (Iterable<String>)
     */
    private void printMenu(Iterable<String> options) {
        options.forEach(System.out::println);
    }

    /**
     * Prints all the users.
     */
    private void printUsers() {
        controller.findAllUsers().forEach(user -> System.out.println(user.toString()));
    }

    /**
     * Prints information about the logged user.
     */
    private void printLoggedUserInfo() {
        String userInfo = controller.getLoggedUserInfo();

        if (userInfo.isEmpty()) {
            System.out.println("Please log into your account.\n");
        } else {
            System.out.println("Logged User: " + userInfo);
        }
    }

    /**
     * Prompts for information about a new user and adds him to the user base.
     */
    private void addUser() {
        Scanner scanner = new Scanner(System.in);
        String firstName, lastName, email;

        System.out.println("Type the user's last name: ");
        lastName = scanner.nextLine();

        System.out.println("Type the user's first name: ");
        firstName = scanner.nextLine();

        System.out.println("Type the user's email address: ");
        email = scanner.nextLine();

        try {
            controller.saveUser(firstName, lastName, email);
            System.out.println("User added successfully!\n");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Prompts for information about the user to be removed and removes him.
     */
    private void removeUser() {
        printUsers();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Type the email of the user you want to remove: ");
        String email = scanner.nextLine();

        try {
            controller.removeUser(email);
            System.out.println("User deleted successfully!\n");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Returns a valid option selected by the user from a set of options.
     *
     * @param options - set of strings containing options
     * @return op - option string
     */
    private String getOption(Set<String> options) {
        Scanner scanner = new Scanner(System.in);
        String option = scanner.nextLine();

        if (option.equals(Constants.EXIT_COMMAND)) {
            return option;
        }

        for (String opt : options) {
            if (opt.equals(option)) {
                return option;
            }
        }

        return null;
    }

    /**
     * Prompts the user for login.
     */
    private void login() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Type your email address: ");

        try {
            String email = scanner.nextLine();
            controller.login(email);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Logs out current user.
     */
    private void logout() {
        controller.logout(); //TODO: implement logout exceptions
    }

    /**
     * Adds friends for logged user.
     */
    private void addFriends() {
        printUsers();

        System.out.println("Type the email address of the user you want to be friend with: ");
        Scanner scanner = new Scanner(System.in);

        try {
            String friendEmail = scanner.nextLine();
            controller.saveFriendship(friendEmail);

            System.out.println("You have sent a friend request!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Remove friends for logged user.
     */
    private void removeFriends() {
        printFriends();

        System.out.print("Type the email address of the friend you want to remove: ");
        Scanner scanner = new Scanner(System.in);

        try {
            String friendEmail = scanner.nextLine();
            controller.removeFriendship(friendEmail);

            System.out.println("Friend removed.");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Prints all friends for logged user.
     */
    private void printFriends() {
        try {
            Iterable<FriendshipDTO> friends = controller.getFriendsOfLoggedUser();

            System.out.println("Your friends: ");

            friends.forEach(friend -> System.out.println(friend.toString()));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Prints the number of communities that exist in the social network.
     */
    private void printNumberOfCommunities() {
        System.out.println("Number of communities: " + controller.getNumberOfCommunities());
    }

    /**
     * Prints the members and longest friend chain for the most sociable community.
     */
    private void printMostSociableCommunity() {
        System.out.println("Members of the most sociable community: ");

        controller.getMostSociableCommunityMembers().forEach((member) -> {
            System.out.println(member.toString());
        });

        System.out.print("Friend chain: ");
        Iterable<User> chain = controller.getMostSociableCommunityChain();
        Iterator it = chain.iterator();

        while (it.hasNext()) {
            User user = (User) it.next();
            System.out.print("(" + user.getID() + ", " + user.getFirstName() + ", " + user.getLastName() + ")");

            if (it.hasNext()) {
                System.out.print(" - ");
            }
        }
        System.out.println();
    }

    private String getUserInput(String promptMessage){
        if(promptMessage!=null) {
            System.out.println(promptMessage);
        }
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private void openConversationsMenu(){
        if(!controller.userIsLoggedIn()){
            //System.out.println("Please log into your account.\n");
            return;
        }

        System.out.println("\n");
        System.out.println("Your conversations:\n ");
        Iterable<ConversationHeaderDTO> conversationHeaders = controller.getConversationHeaders();

        conversationHeaders.forEach(header -> System.out.println(header.toString() + "\n"));
        String option = getUserInput("Type the email of the person you want to chat with.\n");

        if(!option.equals(Constants.EXIT_COMMAND)){
            try {
                ChatRoomUI chatRoomUI = new ChatRoomUI(controller.createChatRoom(option));
                chatRoomUI.open();
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void sendMessageToMultipleUsers(){
        if(!controller.userIsLoggedIn()){
            return;
        }

        System.out.println("Users:\n");
        controller.getConversationHeaders().forEach(header -> System.out.println(header.toString() + "\n"));
        String userList = getUserInput("Type the emails of the users separated by space you want to send the message to: ");
        String message = getUserInput("\nType the message: ");

        try {
            controller.sendMessageToMultipleUsers(message, userList);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void printFriendsRequests() {
        if(!controller.userIsLoggedIn()) {
            return;
        }

        System.out.println("Your requests:\n");
        Iterable<FriendRequestDTO> friendRequestDTOS = controller.getAllFriendRequests();
        friendRequestDTOS.forEach(System.out::println);

        System.out.println("\nChoose what do you want to do: \n1. Accept a request\n2.Decline a request\nPress 0 to exit to main menu");
        Scanner scanner = new Scanner(System.in);

        try {
            String optiune = scanner.nextLine();

            if (optiune.equals("1")) {
                System.out.println("Please enter sender's email: ");
                String email = scanner.nextLine();
                controller.approvedStatusFriend(email);

            } else if (optiune.equals("2")) {
                System.out.println("Please enter sender's email: ");
                String email = scanner.nextLine();
                controller.rejectStatusFriend(email);

            } else if(optiune.equals(Constants.EXIT_COMMAND)) {
                return;
            } else {
                System.out.println("Invalid option!");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void printFriendshipsFromDate(){
        if(!controller.userIsLoggedIn()){
            return;
        }

        String yearStr = getUserInput("Type the year (yyyy): ");
        String month = getUserInput("Type the month (ex: JULY): ");
        int year;

        try{
            year = Integer.parseInt(yearStr);
            Iterable<FriendshipDTO> friendships  = controller.getFriendshipsFromDate(year, month);

            friendships.forEach(System.out::println);
        }catch (NumberFormatException e) {
            System.out.println("Invalid year. Year must be a number!");
        }
    }

    /**
     * Main function that runs the app.
     */
    public void run() {
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("1", this::addUser);
        commands.put("2", this::removeUser);
        commands.put("3", this::printUsers);
        commands.put("4", this::login);
        commands.put("5", this::logout);
        commands.put("6", this::addFriends);
        commands.put("7", this::removeFriends);
        commands.put("8", this::printFriends);
        commands.put("9", this::printNumberOfCommunities);
        commands.put("10", this::printMostSociableCommunity);
        commands.put("11", this::openConversationsMenu);
        commands.put("12", this::sendMessageToMultipleUsers);
        commands.put("13", this::printFriendsRequests);
        commands.put("14", this::printFriendshipsFromDate);

        Iterable<String> menu = generateMenu();

        while (true) {
            printLoggedUserInfo();
            printMenu(menu);
            String option = getOption(commands.keySet());

            if (option == null) {
                System.out.println("Invalid option!\n");

            } else if (option.equals(Constants.EXIT_COMMAND)) {
                break;

            } else {
                commands.get(option).run();
            }
        }
    }
}
