package com.pelr.domain.validators;

import com.pelr.domain.Friendship;

/**
 * FriendshipValidator class
 */

public class FriendshipValidator implements Validator<Friendship>{

    /**
     * Validates friendship
     * @param entity - friendship entity
     * @throws ValidationException if friendship is invalid
     */
    @Override
    public void validate(Friendship entity) throws ValidationException {


    }
}
