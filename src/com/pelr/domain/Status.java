package com.pelr.domain;

public enum Status
{
    PENDING,
    APPROVED,
    REJECTED

}
