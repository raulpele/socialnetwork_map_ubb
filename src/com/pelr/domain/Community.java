package com.pelr.domain;

public class Community {

    private Graph communityGraph;

    public Community(Graph communityGraph){
        this.communityGraph = communityGraph;
    }

    public Iterable<Long> getMemberIDs(){
        return communityGraph.getNodes();
    }
}
