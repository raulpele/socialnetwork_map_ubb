package com.pelr.domain.DTOs;

import java.time.LocalDateTime;

/**
 * FriendshipDTO entity class
 */
public class FriendshipDTO
{
    private String firstName;
    private String lastName;
    private String date;

    /**
     * create a FriendshippDTO entity
     */
    public FriendshipDTO(String firstName, String lastName, String date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
    }

    /**
     *
     * @return a string with the parameters
     */
    @Override
    public String toString() {
        return "First name : " + firstName + "\n" + "Last name: " + lastName + "\n" + "Date: " + date + "\n";

    }
}
