package com.pelr.domain.DTOs;

public class FriendRequestDTO
{
    private String firstName;
    private String lastName;
    private String email;
    private String status;

    public FriendRequestDTO(String firstName, String lastName, String email, String status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Friend request from:\n" +
                "First name: " + firstName + '\n' +
                "Last name: " + lastName + '\n' +
                "Email: " + email + '\n' +
                "Status: " + status + '\n';
    }
}
