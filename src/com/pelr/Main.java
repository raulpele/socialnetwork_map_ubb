package com.pelr;

import com.pelr.UI.Console;
import com.pelr.domain.Friendship;
import com.pelr.domain.Message;
import com.pelr.domain.Tuple;
import com.pelr.domain.User;
import com.pelr.domain.validators.FriendshipValidator;
import com.pelr.domain.validators.UserValidator;
import com.pelr.repository.Repository;
import com.pelr.repository.database.FriendshipDBRepository;
import com.pelr.repository.database.MessageDBRepository;
import com.pelr.repository.database.UserDBRepository;
import com.pelr.service.*;
import com.pelr.utils.Credentials;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Main class that runs the application
 */

public class Main {

    /**
     * Main method that runs the application.
     * @param args - command line arguments
     */

    public static void main(String[] args) {
        UserDBRepository userRepository = new UserDBRepository(Credentials.DB_URL, Credentials.DB_USERNAME, Credentials.DB_PASSWORD, new UserValidator());
        Repository<Tuple<Long, Long>, Friendship> friendshipRepository = new FriendshipDBRepository(Credentials.DB_URL,
                Credentials.DB_USERNAME, Credentials.DB_PASSWORD, new FriendshipValidator());

        MessageDBRepository messageRepository = new MessageDBRepository(Credentials.DB_URL, Credentials.DB_USERNAME, Credentials.DB_PASSWORD);

        UserService userService = new UserService(userRepository);
        FriendshipService friendshipService = new FriendshipService(friendshipRepository);
        Authentication authentication = new Authentication(userRepository);
        MessagingService messagingService = new MessagingService(messageRepository);

        Controller controller = new Controller(userService, friendshipService, authentication, messagingService);
        Console console = new Console(controller);

        console.run();

    }
}
