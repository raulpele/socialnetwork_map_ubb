package com.pelr.service;

/**
 * FriendshipException exception class
 */

public class FriendshipException extends RuntimeException{

    /**
     * Creates a FriendshipException exception with specified message.
     * @param message - message string
     */

    public FriendshipException(String message) {
        super(message);
    }
}
