package com.pelr.service;

public class MessageNotFound extends RuntimeException{
    public MessageNotFound(String message) {
        super(message);
    }
}
